<?php

return [
    'typoscript' => [
        \KITT3N\Kitt3nUserfuncs\ExpressionLanguage\CustomTyposcriptConditionProvider::class
    ],
];
