<?php
namespace KITT3N\Kitt3nUserfuncs\UserFunc;

use Symfony\Component\Filesystem\Filesystem;

class FileExistsUserFunc
{

    /**
     * @param $sFilePathFromDocRoot
     * @return bool
     */
    public static function checkIfFileExists($sFilePathFromDocRoot){

        $sDocumentTargetPath = '';
        $sServerDocumentRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/';
        $fs = new Filesystem();


        if(strpos($sFilePathFromDocRoot, 'EXT:') !== false){
            $sFilePathFromDocRoot = 'typo3conf/ext/' . str_replace('EXT:','',$sFilePathFromDocRoot);
        } else {
            $sFilePathFromDocRoot = ltrim($sFilePathFromDocRoot, '/');
        }

        $sDocumentTargetPath .= $sServerDocumentRoot . $sFilePathFromDocRoot;

        if ($fs->exists($sDocumentTargetPath)) {
            return true;
        } else {
            return false;
        }
    }

}
